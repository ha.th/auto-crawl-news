from elasticsearch import Elasticsearch as client_elastic
import os

LOCAL_HOST_NAME = "http://localhost"
LOCAL_PORT = "9200"

SERVER_HOST_NAME = "http://54.68.196.78"
SERVER_PORT = "9200"
SERVER_AUTH_USER = "elastic"
SERVER_AUTH_PASS = "elasticbk"

# directory path
system_config = "system_config.json"
pipeline_config = "pipeline_config.json"


# call local or server elastic search

def local_elastic():
    return client_elastic(hosts=[LOCAL_HOST_NAME], port=LOCAL_PORT, timeout=90)


def server_elastic():
    return client_elastic(hosts=[SERVER_HOST_NAME],
                          http_auth=(SERVER_AUTH_USER, SERVER_AUTH_PASS),
                          port=SERVER_PORT, timeout=90)


def create_client_elastic_search(type):
    if type == LOCAL_HOST_NAME:
        return local_elastic()
    elif type == SERVER_HOST_NAME:
        return server_elastic()
    else:
        return None


# pattern for each key tag
pattern_search = {
    "deal": [
        "bán ra (.*?) (((\d{1,}.)?)+\d{3,}.\d{3,}(-((\d{1,}.)?)+\d{3,}.\d{3,})?)",
        "mua vào (.*?) (\d{1,}) triệu cổ phiếu",
        "khối lượng (.*?) (\d{1,})",
        "chiết khấu",
        "khối lượng bán ra nhiều",
        "khối lượng bán ra ít ",
        "giá cao",
        "mất giá",
        "mức mua vào",
    ],
    "Company": [
        
    ],
    "StockID": [
    
    ]
}
pattern_key = {
    "deal":
        [
            "giá bán",
            "giá mua",
            "lượng bán",
            "lượng mua",
        ],
    "Company":
        [
            
        ],
    "StockID":
        [
        ]
}

# index of elastic search server
talent_crawled_index = "talent-crawled"
talent_cleaned_index = "talent-cleaned"
