import scrapy
import time
from talent_crawl_data.utils.CrawlUtils import initial_index, scrape_google, filter_exist_url, append_file, scrape_bing
from log.log_service import LogService

KEY_STORE = [
    {'keyword': "chứng khoán", 'file_store': 'url/chungkhoan.url'},
    {'keyword': "cổ phiếu", 'file_store': 'url/cophieu.url'},
    {'keyword': "nhà đầu tư", 'file_store': 'url/nhadautu.url'},
    {'keyword': "giao dịch cổ phiếu", 'file_store': 'url/giaodichcophieu.url'},
    {'keyword': "tin tức chứng khoán", 'file_store': 'url/tintuchungkhoan.url'},
]

USER_AGENT = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'}


class TalentUrlSpider(scrapy.Spider):
    name = 'TalentUrlSpider'
    start_urls = ['https://www.bing.com/']
    pipelines = ['TalentUrlPipeline']

    def __init__(self, domain=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.key_store = KEY_STORE
        self.user_agent = USER_AGENT
        self.last_index = [{"keyword": file['keyword'],
                            "last_index": initial_index(file['file_store']),
                            "file_store": file['file_store']
                            }
                           for file in self.key_store]
        self.domain = domain
        self.crawl_log = LogService().configLogCrawlLink()

    def parse(self, response):
        keywords = [keyword['keyword'] for keyword in KEY_STORE]
        datas = []
        for keyword in keywords:
            try:
                results = scrape_bing('site:'+self.domain+' '+keyword, 10, "en", self.user_agent)
                for result in results:
                    datas.append(result)
            except Exception as e:
                self.logger.info(e)
            finally:
                time.sleep(10)
        for keyword in keywords:
            file_store = [file['file_store'] for file in self.key_store if (file['keyword'] == keyword)][0]
            last_index = [file['last_index'] for file in self.last_index if (file['keyword'] == keyword)][0]

            # lọc các url đã tồn tại và lưu các url mới
            data_keyword = [data for data in datas if (data['keyword'] == 'site:'+self.domain+' '+keyword)]
            filter_data = filter_exist_url(data_keyword, file_store)
            # print("filter")
            # print(len(filter_data))
            # for item in filter_data:
            #     print(item)
            last_index = append_file(filter_data, file_store, last_index, self.crawl_log)

            # đánh lại chỉ mục sau khi lưu
            for file in self.last_index:
                if file['file_store'] == file_store:
                    file['last_index'] = last_index
