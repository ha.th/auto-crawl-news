# -*- coding: utf-8 -*-
import scrapy
from .TalentUrlSpider import TalentUrlSpider
from ..utils.CrawlUtils import initial_index

KEY_STORE = [
    {'keyword': "chứng khoán", 'file_store': 'url/chungkhoan.url'},
    {'keyword': "cổ phiếu", 'file_store': 'url/cophieu.url'},
    {'keyword': "nhà đầu tư", 'file_store': 'url/nhadautu.url'},
    {'keyword': "giao dịch cổ phiếu", 'file_store': 'url/giaodichcophieu.url'},
    {'keyword': "tin tức chứng khoán", 'file_store': 'url/tintuchungkhoan.url'}
]

USER_AGENT = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'}


class Talent1SpiderSpider(TalentUrlSpider):
    name = 'Talent1Spider'
    start_urls = ['http://www.google.com/']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.key_store = KEY_STORE
        self.user_agent = USER_AGENT
        self.last_index = [{"keyword": file['keyword'],
                            "last_index": initial_index(file['file_store']),
                            "file_store": file['file_store']
                            }
                           for file in self.key_store]
