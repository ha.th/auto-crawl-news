## Các bước thực hiện dự án:
- Mỗi người tự cài đặt elasticsearch lên local để sử dụng ( khuyến khích mọi người sử dụng linux nếu dùng window nên dùng wsl)
- Dự án được thực hiện trong folder TALENTDATACRAWL. 

### Bước 1: Thu thập thông tin:

- Crawl các bài báo trên intertet dựa trên 1 số query trên search engine như: "đầu tư chứng khoán", "cổ phiếu",  "chứng khoán", "thị trường chứng khoán"
- Lấy các dữ liệu thô từ các link bài báo: title, headline (abstract), content, source, published_date, images
- Lưu dữ liệu vào elasticsearch index (dùng md5 hash của url làm id). Cần học cách index, query, thiết lập mapping các document trên nền tảng elasticsearch để phục vụ việc dễ dàng truy query, aggregate data. 

### Bước 2: Phân tích, xử lý thông tin:

Xây dựng các modul xử lý dữ liệu. Các bài báo "thô" sẽ được xử lý qua 1 data pipleline gồm các bước:

- Phát hiện bài báo không liên quan  (Filter Processor): nếu bài báo không liên quan gì đến vấn đề "chứng khoán", "đầu tư chứng khoán", quá ít nội dung (video) thì lọc bỏ.
- Phát hiên trùng lặp (Deduplication Processor): nếu 2 bài báo có nội dung rất giống nhau, ko mang lại thêm thông tin mới cho người dùng thì được nhóm vào làm 1
- Phát hiện thông tin liên quan đến mã cổ phiếu, công ty niêm yết : Ví dụ như cổ phiếu VNM - Vinamilk, Ngân hàng VP Bank. Tạo NER để phát hiện các thông tin liên quan đến công ty mã chứng khoán niêm yết.
- Phát hiện các lĩnh vực nhắc đến trong bài báo (Categorization): Ví dụ Chứng khoán, Đầu tư - Tài chính, Ngân hàng, ... (Cần 1 lập 1 bộ category cho dự án.)
- Xử lý tính toán sentiment scoring với các bài báo ảnh hưởng đến thông qua việc count số từ loại positive, negative, neutral. Công thức Score of document = count (pos.matches) – count (neg.matches). Cần thiết lập từ điển cho vấn đề này.

Cuối bước này các bài báo qua xử lý được lưu vào một elasticsearch index mới để để phân tích và tìm kiếm. Sinh viên cần tìm hiểu một số thực viện hỗ trợ NER. Ví dụ https://spacy.io/api/entityrecognizer hay một thư viện phù hợp với việc xử lý tiếng Việt.

### Bước 3: Xuất bản thông tin:

Lấy các bài báo từ bước 3 tự động gửi lên 1 site dùng API. Yêu cầu tổ chức cách trình thông tin cho đẹp mắt, dễ hiểu, đễ tìm và phân tích thông tin. 
Thiết lập API cho text classification với 3 class : positive, negative and neutral cho bên stock trading signal 

### Lưu ý:

